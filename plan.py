#!usr/bin/env python3

import schedule
import time
import subprocess
import os
from tkinter import Tk, messagebox

def job():
    # Vérifier si aujourd'hui est un jour ouvrable (lundi-vendredi)
    if time.localtime().tm_wday < 5:  # tm_wday commence à 0 pour lundi et se termine à 6 pour dimanche
        # Vérifier si l'heure actuelle est 17h (heure locale)
        if time.localtime().tm_hour == 17:
             # Initialise une fenêtre Tkinter
            root = Tk()
            root.withdraw()  # Cache la fenêtre principale
            """Fonction pour exécuter le script Bash et demander une confirmation."""
            response = messagebox.askyesno("Confirmation", "Voulez-vous exécuter le script de push sur Git ?")
            if response :
                script_path = "togit.sh"  # Chemin d'accès au script Bash
                if not os.path.exists(script_path):
                    print(f"Le script bash {script_path} n'existe pas.")
                    return
                if not os.access(script_path, os.X_OK):
                    print(f"Le script bash {script_path} n'est pas exécutable.")
                    return

                print("Exécution du script...")
                try:
                    # changer le chemin vers git-bash.exe
                    git_bash_path = r"C:\Program Files\Git\git-bash.exe"
                    result = subprocess.run([git_bash_path, script_path], check=True, shell=True)  # Exécuter le script Bash
                    print(result.stdout)
                    if result.returncode == 0:
                        print("Script exécuté avec succès.")
                    else:
                        print(f"Une erreur s'est produite lors de l'exécution du script. Code de retour : {result.returncode}")
                        print(result.stderr)
                except subprocess.CalledProcessError as e:
                    print(f"Erreur lors de l'exécution du script : {e.output}")
            else:
                print("Opération annulée.")

# Planifier l'exécution du script tous les jours ouvrables (du lundi au vendredi) à 17h
schedule.every().monday.at("17:00").do(job)
schedule.every().tuesday.at("17:00").do(job)
schedule.every().wednesday.at("17:00").do(job)
schedule.every().thursday.at("17:00").do(job)
schedule.every().friday.at("17:00").do(job)

# Afficher un message et attendre la saisie de l'utilisateur
print("Tâche planifiée. Appuyez sur Ctrl+C pour quitter.")
while True:
    schedule.run_pending()
    time.sleep(1)  # Attendre une seconde avant de vérifier les tâches en attente