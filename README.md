# Planification Automatique de Push Git

Ce projet vise à automatiser le processus de push vers un dépôt GitLab à une heure spécifique chaque jour de la semaine.

## Prérequis

* Un compte GitLab avec les autorisations de push sur le dépôt concerné.
* Une clé SSH configurée pour l'authentification.
* Python 3 installé sur le système avec les dépendances requises.

## Installation

1. **Génération et Configuration de la Clé SSH :**

Pour générer une paire de clés SSH, ouvrez un terminal et exécutez la commande suivante :

```bash
ssh-keygen -t rsa -b 4096 -C "votre_email@example.com"
```
Suivez les instructions à l'écran pour générer la clé. Une fois la clé générée, ajoutez-la à l'agent SSH en exécutant :

```bash
eval "$(ssh-agent -s)"
ssh-add ~/.ssh/id_rsa
```
Copiez ensuite la clé publique dans le presse-papiers avec :

```bash
cat ~/.ssh/id_rsa.pub
```
Collez ensuite la clé publique dans les paramètres SSH de votre compte GitLab.

2. **Installation de Python 3 :**

Si Python 3 n'est pas installé sur votre système, vous pouvez le télécharger à partir du site officiel Python : [Télécharger Python](https://www.python.org/downloads/).

ou

```bash
sudo apt update
sudo apt install python3
```
Après l'installation, vous pouvez vérifier que Python 3 est bien installé en exécutant la commande :

```bash
python3 --version
```

Assurez-vous également d'avoir les dépendances Python nécessaires installées en exécutant :

```bash
pip install schedule tkinter
```

Clonage du Dépôt :

Clonez ce dépôt sur votre machine locale en utilisant SSH :

```bash
git clone git@gitlab.com:Amyn00/autogitplan.git
```

## Utilisation

1. Configurez les fichiers `batchfile.bat`, `plan.py`, et `togit.sh` selon vos besoins spécifiques.

2. Exécutez `batchfile.bat` pour lancer la planification automatique du push Git.

3. Le script `plan.py` sera exécuté à l'heure spécifiée chaque jour ouvrable (du lundi au vendredi). Il demandera une confirmation avant de procéder au push.

4. Le script `togit.sh` gère le processus de push. Il vérifie les mises à jour depuis la branche principale, effectue les commits nécessaires et pousse les modifications vers le dépôt GitLab.

## Configuration de la Planification

La planification est définie dans le fichier `plan.py`. Actuellement, elle est configurée pour exécuter le script de push tous les jours ouvrables à 17h.

## Licence

Ce projet est distribué sous la licence MIT.
