#!/usr/bin/env bash

# Démarrer l'agent SSH et ajouter la clé
eval "$(ssh-agent -s)"
ssh-add ~/.ssh/my_rsa

# Définir le chemin du fichier journal
log_file="error_log.txt"

# Fonction pour enregistrer les erreurs dans le fichier journal
log_error() {
    echo "Erreur: $1" >> "$log_file"
}

# Rediriger la sortie standard et d'erreur vers le fichier journal
exec > >(tee -a "$log_file") 2>&1

# Aller sur votre branche de travail (feat/branchname)
read -p "Saisir votre nom de branche feat/ : " branchname
git checkout feat/"$branchname" || { log_error "Impossible de basculer sur la branche feat/$branchname" "$?"; exit 1; }

# Vérifier s'il y a des modifications sur la branche distante main
if git diff --quiet origin/main; then
  echo "Votre branche est à jour avec la branche main."
else
  read -p "Votre branche est en retard par rapport à la branche main. Voulez-vous récupérer les dernières modifications de la branche main ? (o/n) " response
  if [[ "$response" =~ ^([oO]|[yY])+$ ]]; then
    git pull origin main || { log_error "Impossible de récupérer les modifications de la branche main" "$?"; exit 1; }
  else
    echo "Opération annulée. Assurez-vous de synchroniser votre branche avant de pousser."
    exit 1
  fi
fi

git add .

read -p "Écrivez votre message de commit ici : " msg
if [ -z "$msg" ]; then
  echo "Veuillez saisir votre message de commit."
  exit 1
fi
git commit -m "$msg" || { log_error "Impossible de réaliser le commit" "$?"; exit 1; }

git push origin feat/"$branchname" || { log_error "Impossible de pousser vers la branche feat/$branchname" "$?"; exit 1; }

echo "Opérations terminées avec succès."

# Attendre que l'utilisateur appuie sur une touche avant de quitter
read -n 1 -s -r -p "Appuyez sur n'importe quelle touche pour quitter..."
